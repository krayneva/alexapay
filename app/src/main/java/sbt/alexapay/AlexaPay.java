package sbt.alexapay;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import sbt.alexapay.di.AppComponent;
import sbt.alexapay.di.DaggerAppComponent;
import sbt.alexapay.di.modules.ContextModule;

/**
 * Created by irina on 31.03.17.
 */

public class AlexaPay extends Application {
    public static final String TAG = AlexaPay.class.getSimpleName();
    public static AlexaPay instance;


    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        sAppComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .build();

    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }



    @VisibleForTesting
    public static void setAppComponent(@NonNull AppComponent appComponent) {
        sAppComponent = appComponent;
    }

    public static AlexaPay getInstance(){
        if (instance==null){
            instance = new AlexaPay();
        }
        return instance;
    }

    public static void log(String message){
        Log.w(TAG,message);
    }


    public static Context getContext(){
        return getInstance().getApplicationContext();
    }
}
