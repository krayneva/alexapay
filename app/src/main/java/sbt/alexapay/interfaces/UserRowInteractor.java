package sbt.alexapay.interfaces;

import android.view.View;

/**
 * Created by irina on 25.04.17.
 */

public interface UserRowInteractor {
    void onDeleteUserClick(View v);
}
