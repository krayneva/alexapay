package sbt.alexapay.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sbt.alexapay.connectivity.ServerAPI;
import sbt.alexapay.connectivity.ServerService;

/**
 * Date: 8/26/2016
 * Time: 11:58
 *
 * @author Artur Artikov
 */

@Module(includes = {ApiModule.class})
public class ServerModule {
	@Provides
	@Singleton
	public ServerService provideServerService(ServerAPI api) {
		return new ServerService(api);
	}
}