package sbt.alexapay.utils;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;

import sbt.alexapay.AlexaPay;


/**
 * Created by irina on 20.04.17.
 */

public class PermissionsManager {

    static String[] permissions = {
            Manifest.permission.INTERNET,
            Manifest.permission.READ_CONTACTS

    };

    public static  String[] checkIfPermissions(){
        ArrayList<String> ungranted = new ArrayList<String>();
        for (String perm: permissions){
            if (ContextCompat.checkSelfPermission(AlexaPay.getContext(),perm) != PackageManager.PERMISSION_GRANTED){
                ungranted.add(perm);
                Log.w("permission", "ungranted: " + perm);
            }
        }

        String[] arr = new String[ungranted.size()];
        for(int i=0; i< ungranted.size(); i++){
            arr[i] = ungranted.get(i);
        }
        return arr;
    }


}
