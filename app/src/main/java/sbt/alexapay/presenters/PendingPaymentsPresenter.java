package sbt.alexapay.presenters;

import android.text.TextUtils;
import android.util.Base64;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.common.Utils;
import sbt.alexapay.connectivity.ServerService;
import sbt.alexapay.managers.PaymentsManager;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.views.PendingPaymentsView;

/**
 * Created by irina on 27.04.17.
 */
@InjectViewState
public class PendingPaymentsPresenter extends BasePresenter<PendingPaymentsView> {
    @Inject
    ServerService server;
    public PendingPaymentsPresenter() {
        AlexaPay.getAppComponent().inject(this);
    }


    public void updatePayments() {
        getViewState().showProgress("Загрузка платежей...");

      Subscription subscription = server.updateTransfers()
                //TODO READ
                //.doOnNext(list-> {
                 //   AlexaPay.log("List size is "+list.size());})
                //TODO READ
                 .compose(Utils.applySchedulers())
                .subscribe(list -> {
                    AlexaPay.log("received list "+list.toString());
                    PaymentsManager.getInstance().reformPayments(list);
                    getViewState().updatePaymentList();
                    getViewState().dismissProgress();
                }, exception -> {
                    getViewState().dismissProgress();
                    //TODO прокнуть сюда смыКупчинская, 21К1сл ошибки
                    getViewState().showError("Произошла ошибка");
                });

        unsubscribeOnDestroy(subscription);
    }


    public void acceptPayment(Transfer transfer){
        getViewState().showProgress("Подтверждение платежа...");

        Subscription subscription = server.acceptTransfer(transfer)
                //TODO READ
                //.doOnNext(user -> AuthUtils.setToken(token))
                //TODO read
                .compose(Utils.applySchedulers())
                .subscribe(v -> {
                    getViewState().updatePaymentList();
                    getViewState().dismissProgress();
                }, exception -> {
                    //TODO прокнуть сюда смысл ошибки
                    getViewState().showError("Произошла ошибка");
                });
        unsubscribeOnDestroy(subscription);
    }

    public void declinePayment(Transfer transfer){
        getViewState().showProgress("Отклонение платежа...");
        Subscription subscription = server.declineTransfer(transfer)
                //TODO READ
                //.doOnNext(user -> AuthUtils.setToken(token))
                //TODO read
                // .compose(Utils.applySchedulers())
                .subscribe(v -> {
                    getViewState().updatePaymentList();
                    getViewState().dismissProgress();
                }, exception -> {
                    //TODO прокнуть сюда смысл ошибки
                    getViewState().showError("Произошла ошибка");
                });
        unsubscribeOnDestroy(subscription);

    }

}
