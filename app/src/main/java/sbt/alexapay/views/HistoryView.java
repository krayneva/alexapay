package sbt.alexapay.views;

import com.arellomobile.mvp.MvpView;

/**
 * Created by Dell on 29.04.2017.
 */

public interface HistoryView extends MvpView {
    void updatePaymentList();
    void showProgress(String message);
    void dismissProgress();
    void showError(String message);
    void showMessage(String message);
}
