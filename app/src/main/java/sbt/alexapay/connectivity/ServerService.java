package sbt.alexapay.connectivity;

import java.util.List;

import rx.Observable;
import sbt.alexapay.objects.Transfer;

/**
 * Created by irina on 27.04.17.
 */

public class ServerService {
    private ServerAPI serverAPI;

    public ServerService(ServerAPI serverAPI) {
        this.serverAPI = serverAPI;
    }


    public Observable<List<Transfer>> updateTransfers() {
        return serverAPI.getPayments();
    }
    public Observable<Void> acceptTransfer(Transfer transfer) {
        return serverAPI.acceptPayment(transfer.id);
    }
    public Observable<Void> declineTransfer(Transfer transfer) {
        return serverAPI.declinePayment(transfer.id);
    }



}
