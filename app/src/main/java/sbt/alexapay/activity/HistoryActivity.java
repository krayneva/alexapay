package sbt.alexapay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbt.alexapay.adapters.PaymentsAdapter;
import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.managers.PaymentsManager;
import sbt.alexapay.R;
import sbt.alexapay.presenters.HistoryPresenter;
import sbt.alexapay.presenters.PendingPaymentsPresenter;
import sbt.alexapay.views.PendingPaymentsView;

public class HistoryActivity extends BaseActivity  implements View.OnClickListener, PendingPaymentsView
{
    @InjectPresenter
    HistoryPresenter presenter;
    @BindView(R.id.listViewPayments)
    public ListView paymentsList;
    @BindView(R.id.buttonRefresh)
    public Button refreshButton;

    private PaymentsAdapter adapter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMenuDrawer();
        bindWidgets();
    }

    private void bindWidgets(){
        ButterKnife.bind(this);
        refreshButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePayments();
    }
    @Override
    public void updateUI(){
        updatePaymentList();
    }


    private void updatePayments(){
        showProgress("Загрузка списка платежей...");
      //  HttpRequester.getInstance().getPayments();
        presenter.updatePayments();
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.buttonRefresh:
                updatePayments();
                break;
            default:
                showToast("Onclick is not defined");
        }
    }




    @Override
    public void updatePaymentList() {
        adapter = new PaymentsAdapter(this, PaymentsManager.getInstance().executedPayments);
        paymentsList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


}
