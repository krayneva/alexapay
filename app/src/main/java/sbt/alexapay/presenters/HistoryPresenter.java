package sbt.alexapay.presenters;

/**
 * Created by irina on 28.04.17.
 */

import com.arellomobile.mvp.InjectViewState;


import javax.inject.Inject;

import rx.Subscription;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.common.Utils;
import sbt.alexapay.connectivity.ServerService;
import sbt.alexapay.managers.PaymentsManager;

import sbt.alexapay.views.HistoryView;




/**
 * Created by irina on 27.04.17.
 */
@InjectViewState
public class HistoryPresenter extends BasePresenter<HistoryView> {
    @Inject
    ServerService server;
    public HistoryPresenter() {
        AlexaPay.getAppComponent().inject(this);
    }


    public void updatePayments() {
        getViewState().showProgress("Загрузка платежей...");

        Subscription subscription = server.updateTransfers()
                //TODO READ
                //.doOnNext(list-> {
                //   AlexaPay.log("List size is "+list.size());})
                //TODO READ
                .compose(Utils.applySchedulers())
                .subscribe(list -> {
                    AlexaPay.log("received list "+list.toString());
                    PaymentsManager.getInstance().reformPayments(list);
                    getViewState().updatePaymentList();
                    getViewState().dismissProgress();
                }, exception -> {
                    getViewState().dismissProgress();
                    //TODO прокнуть сюда смыКупчинская, 21К1Купчинская, 21К1сл ошибки
                    getViewState().showError("Произошла ошибка");
                });

        unsubscribeOnDestroy(subscription);
    }

}
