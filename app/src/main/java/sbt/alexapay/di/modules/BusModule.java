package sbt.alexapay.di.modules;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import sbt.alexapay.connectivity.ServerAPI;

/**
 * Date: 20.09.2016
 * Time: 20:22
 *
 * @author Yuri Shmakov
 */
@Module
public class BusModule {
	@Provides
	@Singleton
	public Bus provideBus(ServerAPI api) {
		return new Bus();
	}
}
