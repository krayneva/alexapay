package sbt.alexapay.connectivity;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.security.cert.CertificateException;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.objects.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sbt.alexapay.AlexaPay;

/**
 * Created by irina on 31.03.17.
 */

public class HttpRequester {
    private static final String TAG = AlexaPay.TAG + " " + HttpRequester.class.getSimpleName();
    private static HttpRequester instance;
    private Retrofit retrofit;
    private ServerAPI api;
    public static final String SERVER_ADDRESS = "http://sbertech.herokuapp.com";


    public static final String EVENT_ERROR = "Error response";

    private HttpRequester(){
        retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_ADDRESS)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();
        api = retrofit.create(ServerAPI.class);
    }

    public static HttpRequester getInstance(){
        if (instance==null){
            instance = new HttpRequester();
        }
        return instance;
    }


    public void getUsers(){
        Call<List<User>> call = api.getUsers();
        AlexaPay.log(call.request().url().toString());
        call.enqueue(new User());
    }

 /*  public void getPayments(){
        Call<List<Transfer>> call = api.getPayments();
        call.enqueue(new Transfer());

    }*/

 /*   public void acceptPayment(String paymentId){
        Call<Void> call = api.acceptPayment(paymentId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getPayments();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.w(TAG, t.getLocalizedMessage());
            }
        });
    }
*/
    public void addUser(User user){
        Call<Void> call = api.addUser(user);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getUsers();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.w(TAG, t.getLocalizedMessage());
                Intent intent = new Intent(EVENT_ERROR);
                LocalBroadcastManager.getInstance(AlexaPay.getInstance().getContext()).sendBroadcast(intent);
            }
        });

    }

    public void deleteUser(User user){
        Call<Void> call = api.deleteUser(user);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getUsers();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.w(TAG, t.getLocalizedMessage());
                Intent intent = new Intent(EVENT_ERROR);
                LocalBroadcastManager.getInstance(AlexaPay.getInstance().getContext()).sendBroadcast(intent);
            }
        });
    }
/*
    public void declinePayment(String paymentId){
        Call<Void> call = api.declinePayment(paymentId);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getPayments();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.w(TAG, t.getLocalizedMessage());
            }
        });
    }
*/



    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);

            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
