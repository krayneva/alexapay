package utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by SBT-Krayneva-IV on 26.10.2016.
 */
public class RobotoTextView extends android.support.v7.widget.AppCompatTextView {
    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (isInEditMode()) {
            return;
        }
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "RobotoRegular.ttf");
            setTypeface(typeface);
    }

}