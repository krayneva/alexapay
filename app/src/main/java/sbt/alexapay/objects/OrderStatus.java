package sbt.alexapay.objects;

import com.google.gson.annotations.SerializedName;

import sbt.alexapay.R;

/**
 * Created by irina on 03.04.17.
 */

public class OrderStatus {
    @SerializedName("status")
    public int status;
    @SerializedName("name")
    public String name;
    @SerializedName("color")
    public int color;

    public OrderStatus(int value){
        status = value;
        name = getStatusString(value);
        color = getStatusColor(value);
    }


     /*
    1 Заказ зарегистрирован. (Попыток оплаты не было).
    2 Успешный заказ. (Оплата прошла успешно).
    3 Неудачный заказ. (Оплата прошла неуспешно, авторизация отклонена, и т.д.)
    4 Заказ отменен.
     */

    public String getStatusString(int value) {
        switch (value) {
            case 0:
                return "В ожидании";
            case 1:
                return "Подтвержден";
            case 2:
                return "Отклонен";
            default:
                return "Статус неизвестен";
        }
    }

    public  int getStatusColor(int value) {
        switch (value) {
            case 0:
                return R.color.order_status_registered;
            case 1:
                return R.color.order_status_success;
            case 2:
                return R.color.order_status_error;
            default:
                return R.color.order_status_registered;
        }
    }
}

