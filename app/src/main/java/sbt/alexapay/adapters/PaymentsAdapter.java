package sbt.alexapay.adapters;

/**
 * Created by irina on 03.04.17.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sbt.alexapay.objects.OrderStatus;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.R;


/**
 * Created by SBT-Krayneva-IV on 21.10.2016.
 */
public class PaymentsAdapter extends BaseAdapter {
    public static final String TAG = AlexaPay.TAG+" "+PaymentsAdapter.class.getSimpleName();
    private ArrayList<Transfer> payments;
    private Context ctx;
    private LayoutInflater inflater;

    public PaymentsAdapter(Context c, List<Transfer> o) {
        ctx = c;
        payments = new ArrayList<>();
        if (o != null) {
            payments.addAll(o);
        }
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {
        TextView date,amount,status, name ,time;
        int id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        Transfer bear = payments.get(position);
        if ((rowView==null)||(((ViewHolder)rowView.getTag()).id!=bear.id.hashCode())) {
            if (inflater==null)
                inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_payment, null);
            holder = new ViewHolder();
            holder.date = (TextView) rowView.findViewById(R.id.textViewDate);
            holder.amount = (TextView) rowView.findViewById(R.id.textViewAmount);
            holder.status = (TextView) rowView.findViewById(R.id.textViewStatus);
            holder.name = (TextView) rowView.findViewById(R.id.textViewName);
            holder.time = (TextView) rowView.findViewById(R.id.textViewTime);

            holder.id = bear.id.hashCode();
            rowView.setTag(holder);
        }
        else{
            holder = (ViewHolder)rowView.getTag();
        }
        holder.amount.setText("\u20BD"+bear.transfer_amount);
        holder.name.setText(bear.nick);
        OrderStatus orderStatus = new OrderStatus(bear.status);
        holder.status.setText(orderStatus.name);
        holder.status.setTextColor(ctx.getResources().getColor(orderStatus.color));
        holder.amount.setTextColor(ctx.getResources().getColor(orderStatus.color));

        return rowView;


    }

    @Override
    public int getCount() {
        return payments == null ? 0 : payments.size();
    }

    @Override
    public Object getItem(int position) {
        return payments == null ? null : payments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
