package sbt.alexapay.objects;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import sbt.alexapay.managers.PaymentsManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sbt.alexapay.AlexaPay;

/**
 * Created by irina on 31.03.17.
 */

public class Transfer{
    public static final String TAG = AlexaPay.TAG+Transfer.class.getSimpleName();
    public static final String EVENT_PAYMENTS = "PaymetnsUpdatedBroadcast";
    @SerializedName("nick")
    public String nick;
    @SerializedName("id")
    public String id;
    @SerializedName("transfer_amount")
    public int transfer_amount;
    @SerializedName("status")
    public int status;
    @SerializedName("date_of_request")
    public String requestDate;
    @SerializedName("date_of_confirm")
    public String confirmDate;


}