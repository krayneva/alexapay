package sbt.alexapay.di;

import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import sbt.alexapay.connectivity.ServerService;
import sbt.alexapay.di.modules.BusModule;
import sbt.alexapay.di.modules.ContextModule;
import sbt.alexapay.di.modules.ServerModule;
import sbt.alexapay.presenters.HistoryPresenter;
import sbt.alexapay.presenters.PendingPaymentsPresenter;

/**
 * Date: 8/18/2016
 * Time: 14:49
 *
 * @author Artur Artikov
 */
@Singleton
@Component(modules = {ContextModule.class, BusModule.class, ServerModule.class})
public interface AppComponent {
	Context getContext();
	ServerService getServerService();
	Bus getBus();

	void inject(PendingPaymentsPresenter presenter);
	void inject(HistoryPresenter presenter);
}
