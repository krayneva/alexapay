package sbt.alexapay.objects;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.managers.PaymentsManager;

/**
 * Created by irina on 31.03.17.
 */

public class User implements Callback<List<User>> {

    public static final String TAG = AlexaPay.TAG+User.class.getSimpleName();
    public static final String EVENT_USERS = "UsersUpdatedBroadcast";

    public static final ArrayList<User> users = new ArrayList();
    @SerializedName("name")
    public String name;
    @SerializedName("tel_number")
    public long tel_number;
    @SerializedName("nick")
    public String nick;

    public User(){}

    public User(String name, String nick, String phone){
        this.name = name;
        this.nick = nick;
        this.tel_number = Long.parseLong(phone.replaceAll("[^\\d]","").trim());
    }



    @Override
    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
        users.clear();
        if ((response!=null)&&(response.body()!=null))
             users.addAll(response.body());
        Intent intent = new Intent(EVENT_USERS);
        LocalBroadcastManager.getInstance(AlexaPay.getInstance().getContext()).sendBroadcast(intent);
    }

    @Override
    public void onFailure(Call<List<User>> call, Throwable t) {
        Intent intent = new Intent(EVENT_USERS);
        LocalBroadcastManager.getInstance(AlexaPay.getInstance().getContext()).sendBroadcast(intent);
    }
}
