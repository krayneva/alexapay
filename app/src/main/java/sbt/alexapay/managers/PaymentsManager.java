package sbt.alexapay.managers;

import java.util.ArrayList;
import java.util.List;

import sbt.alexapay.objects.Transfer;

/**
 * Created by irina on 03.04.17.
 */

public class PaymentsManager {
    public static final int PAYMENT_STATUS_PENDING = 0;
    public static final int PAYMENT_STATUS_ACCEPTED = 1;
    public static final int PAYMENT_STATUS_DECLINED = 2;

    private static PaymentsManager instance;
    public List<Transfer> pendingPayments = null;
    public List<Transfer> executedPayments = null;

    private PaymentsManager(){
        pendingPayments = new ArrayList<Transfer>();
        executedPayments = new ArrayList<Transfer>();
    }

    public static PaymentsManager getInstance(){
        if (instance==null){
            instance = new PaymentsManager();
        }
        return instance;
    }


    public void reformPayments(List<Transfer> list){
        pendingPayments.clear();
        executedPayments.clear();
        for (Transfer t: list){
            if (t.status==PAYMENT_STATUS_PENDING){
                pendingPayments.add(t);
            }
            else{
                executedPayments.add(t);
            }
        }
    }


}
