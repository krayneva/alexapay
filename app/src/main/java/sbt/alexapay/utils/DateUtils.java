package sbt.alexapay.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Dell on 29.04.2017.
 */

public class DateUtils {
    public static String formatDate(String d){
        Date date = getDateFronGMTString(d);
        String fDate = "";
        if (date==null)
            return fDate;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        return sdf.format(date);
    }


    public static String formatTime(String d){
        Date date = getDateFronGMTString(d);
        String fDate = "";
        if (date==null)
            return fDate;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    public static Date getDateFronGMTString(String date){
        Date fDate = null;
        if ((date==null)||(date.isEmpty())){
            return fDate;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
       // SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss:SSSZ");
        //format.setTimeZone(TimeZone.getTimeZone("GMT"));
        try{
            fDate = format.parse(date);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return fDate;
    }

}
