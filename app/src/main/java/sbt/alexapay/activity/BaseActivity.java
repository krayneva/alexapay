package sbt.alexapay.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;

import net.hockeyapp.android.UpdateManager;

import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.R;
import sbt.alexapay.objects.User;
import sbt.alexapay.utils.PermissionsManager;


/**
 * Created by irina on 31.03.17.
 */

public abstract class BaseActivity extends MvpAppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    public static final String TAG = AlexaPay.TAG+" "+BaseActivity.class.getSimpleName();
    private ProgressDialog progress;
    public static final int REQUEST_PERMISSIONS_CODE = 0;
    private boolean updateReceiverRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void initMenuDrawer(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
        if (checkPermissions()) {
            checkForCrashes();
            checkForUpdates();

        }
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (updateReceiverRegistered){
            unregisterManagers();
            updateReceiverRegistered = false;
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            Intent intent = new Intent(this, PendingPaymentsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(this, HistoryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_users) {
            Intent intent = new Intent(this, UsersActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void showToast(String message){
        Toast toast = new Toast(getBaseContext());
        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE );
        View toastView = inflater.inflate(R.layout.toast, null);
        toast.setView(toastView);
        TextView text = (TextView) toastView.findViewById(R.id.textView);
        text.setText(message);
        toast.show();
    }

    public void showProgress(String message){
        if (!this.isFinishing()) {
            if (progress == null)
                progress = new ProgressDialog(this);
            progress.setMessage(message);
            if (!progress.isShowing()) progress.show();
        }
    }



    public void showError(String message) {
        showToast(message);
    }


    public void showMessage(String message) {
        showToast(message);
    }


    public  void dismissProgress(){
        if ((progress!=null)&&(progress.isShowing()))
            progress.dismiss();
        progress = null;
    }


    private void registerReceiver(){
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        //TODO diff actions for diff activities
        intentFilter.addAction(Transfer.EVENT_PAYMENTS);
        intentFilter.addAction(User.EVENT_USERS);
        intentFilter.addAction(HttpRequester.EVENT_ERROR);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onBroadcastReceived(intent);
        }

    };



    public   void updateUI(){};


    protected void onBroadcastReceived(Intent intent){
        Log.w(TAG,"received broadcast");

        if (intent.getAction()==HttpRequester.EVENT_ERROR){
            dismissProgress();
            showToast("Произошла ошибка");
        }
    }





    protected boolean checkPermissions(){
        String [] ungranted = PermissionsManager.checkIfPermissions();
        if (ungranted.length==0)
            return true;
        String [] a = {ungranted[0]};
        Log.w("permission", "requesting "+ungranted[0]);
        ActivityCompat.requestPermissions(this, a, REQUEST_PERMISSIONS_CODE);
        return false;
    }



    private void checkForCrashes() {
        // CrashManager.register(this, "f95676aee55447a9aa3da927ae9e48f9", new CustomCrashListener());
    }

    private void checkForUpdates() {
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS_CODE: {
                for (int i=0; i< grantResults.length; i++) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        finish();
                    }

                    if (checkPermissions()) {
                        checkForCrashes();
                        checkForUpdates();
                    }

                    return;
                }
            }

        }
    }
}
