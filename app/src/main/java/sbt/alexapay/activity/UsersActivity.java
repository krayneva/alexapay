package sbt.alexapay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import sbt.alexapay.R;
import sbt.alexapay.adapters.PaymentsAdapter;
import sbt.alexapay.adapters.UsersListAdapter;
import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.managers.PaymentsManager;

/**
 * Created by irina on 03.04.17.
 */

public class UsersActivity extends BaseActivity implements View.OnClickListener{
    private LinearLayout addUserLayout;
    private UsersListAdapter adapter;
    private RecyclerView userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        initMenuDrawer();
        bindWidgets();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUsers();
    }


    private void bindWidgets(){
        addUserLayout = (LinearLayout) findViewById(R.id.linearLayoutAddUser);
        addUserLayout.setOnClickListener(this);
        userList = (RecyclerView) findViewById(R.id.listViewUsers);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        userList.setLayoutManager(llm);
        updateUserList();
    }


    private void updateUsers(){
        showProgress("Загрузка списка пользователей...");
        HttpRequester.getInstance().getUsers();
    }

    private void updateUserList(){
        adapter = new UsersListAdapter(this);
        userList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }



    @Override
    protected void onBroadcastReceived(Intent intent) {
        super.onBroadcastReceived(intent);
        updateUserList();
        dismissProgress();
    }


    @Override
    public void updateUI() {
        updateUserList();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.linearLayoutAddUser:
                addUser();
                break;
            default:
                break;
        }
    }


    private void addUser(){
        //showToast("not implemented yet");
        Intent intent = new Intent(UsersActivity.this, AddUserActivity.class);
        startActivity(intent);

    }
}
