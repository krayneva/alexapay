package sbt.alexapay.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.ArrayList;
import java.util.List;

import sbt.alexapay.activity.BaseActivity;
import sbt.alexapay.activity.PendingPaymentsActivity;
import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.AlexaPay;
import sbt.alexapay.R;
import sbt.alexapay.presenters.PendingPaymentsPresenter;
import sbt.alexapay.utils.DateUtils;

/**
 * Created by irina on 03.04.17.
 */

public class PendingPaymentsAdapter  extends BaseAdapter implements View.OnClickListener{


    public static final String TAG = AlexaPay.TAG+" "+PaymentsAdapter.class.getSimpleName();
    private ArrayList<Transfer> payments;
    private LayoutInflater inflater;
    private PendingPaymentsActivity activity;


    public PendingPaymentsAdapter(PendingPaymentsActivity parentActivity, List<Transfer> o) {
        activity = parentActivity;
        payments = new ArrayList<>();
        if (o != null) {
            payments.addAll(o);
        }
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    static class ViewHolder {

        TextView date,amount,status, name ,time;
        ImageButton decline, approve;
        String id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        Transfer bear = payments.get(position);
        if ((rowView==null)||(!((ViewHolder)rowView.getTag()).id.equals(bear.id))) {
            if (inflater==null)
                inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_pending_payment, null);
            holder = new ViewHolder();
            holder.date = (TextView) rowView.findViewById(R.id.textViewDate);
            holder.amount = (TextView) rowView.findViewById(R.id.textViewAmount);
            holder.status = (TextView) rowView.findViewById(R.id.textViewStatus);
            holder.name = (TextView) rowView.findViewById(R.id.textViewName);
            holder.time = (TextView) rowView.findViewById(R.id.textViewTime);
            holder.decline = (ImageButton) rowView.findViewById(R.id.imageButtonDecline);
            holder.decline.setOnClickListener(this);
            holder.decline.setTag(bear);
            holder.approve = (ImageButton) rowView.findViewById(R.id.imageButtonApprove);
            holder.approve.setOnClickListener(this);
            holder.approve.setTag(bear);
            holder.date.setText(DateUtils.formatDate(bear.confirmDate));
            holder.time.setText(DateUtils.formatTime(bear.confirmDate));
            holder.id = bear.id;
            rowView.setTag(holder);
        }
        else{
            holder = (ViewHolder)rowView.getTag();
        }

        holder.amount.setText("\u20BD"+bear.transfer_amount);
        holder.name.setText(bear.nick);

        return rowView;


    }

    @Override
    public void onClick(View v) {
        Transfer transfer = (Transfer)v.getTag();
        if (v.getId() == R.id.imageButtonApprove) {
         activity.acceptPayment(transfer);
        }
        if (v.getId() == R.id.imageButtonDecline) {
            activity.declinePayment(transfer);
        }

    }




    @Override
    public int getCount() {
        return payments == null ? 0 : payments.size();
    }

    @Override
    public Object getItem(int position) {
        return payments == null ? null : payments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
