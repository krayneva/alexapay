package sbt.alexapay.di.modules;



import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import sbt.alexapay.connectivity.ServerAPI;

/**
 * Date: 9/2/2016
 * Time: 18:54
 *
 * @author Artur Artikov
 */
@Module(includes = {RetrofitModule.class})
public class ApiModule {
	@Provides
	@Singleton
	public ServerAPI provideAPI(Retrofit retrofit) {
		return retrofit.create(ServerAPI.class);
	}
}
