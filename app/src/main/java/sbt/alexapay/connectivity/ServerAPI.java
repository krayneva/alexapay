package sbt.alexapay.connectivity;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import rx.Observable;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.objects.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by irina on 31.03.17.
 */

public interface ServerAPI {


    @GET("/users")
    Call<List<User>> getUsers();

    @GET("/transfers")
    Observable<List<Transfer>> getPayments();

    @GET("/accept/{id}")
    Observable<Void> acceptPayment(@Path("id") String id);

    @GET("/decline/{id}")
    Observable<Void> declinePayment(@Path("id") String id);

    @POST("/users")
    Call<Void> addUser(@Body User user);

   // @DELETE("/users")
    @HTTP(method = "DELETE", path = "/users", hasBody = true)
    Call<Void> deleteUser(@Body User user);


}
