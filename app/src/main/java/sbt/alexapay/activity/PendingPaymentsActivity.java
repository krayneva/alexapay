package sbt.alexapay.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import sbt.alexapay.adapters.PaymentsAdapter;
import sbt.alexapay.adapters.PendingPaymentsAdapter;
import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.managers.PaymentsManager;
import sbt.alexapay.R;
import sbt.alexapay.objects.Transfer;
import sbt.alexapay.presenters.PendingPaymentsPresenter;
import sbt.alexapay.views.PendingPaymentsView;

/**
 * Created by irina on 03.04.17.
 */

public class PendingPaymentsActivity extends BaseActivity implements View.OnClickListener, PendingPaymentsView
{
    @InjectPresenter
    PendingPaymentsPresenter presenter;

    @BindView(R.id.listViewPayments)
    public ListView paymentsList;
    @BindView(R.id.buttonRefresh)
    public Button refreshButton;

    private PendingPaymentsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMenuDrawer();
        bindWidgets();
    }

    private void bindWidgets(){
        ButterKnife.bind(this);
        refreshButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.updatePayments();
    }



    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.buttonRefresh:
                presenter.updatePayments();
                break;
            default:
                showToast("Onclick is not defined");
        }
    }



    @Override
    public void updatePaymentList() {
        adapter = new PendingPaymentsAdapter(this, PaymentsManager.getInstance().pendingPayments);
        paymentsList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    //TODO think how to move it to presenter
    public void acceptPayment(Transfer transfer){
        presenter.acceptPayment(transfer);
    }

    public void declinePayment(Transfer transfer){
        presenter.declinePayment(transfer);
    }

}

