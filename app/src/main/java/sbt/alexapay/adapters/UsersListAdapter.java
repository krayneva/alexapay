package sbt.alexapay.adapters;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import sbt.alexapay.activity.BaseActivity;
import sbt.alexapay.connectivity.HttpRequester;
import sbt.alexapay.databinding.UserRowBinding;
import sbt.alexapay.interfaces.UserRowInteractor;
import sbt.alexapay.objects.User;

/**
 * Created by irina on 25.04.17.
 */

public class UsersListAdapter extends  RecyclerView.Adapter {
    private BaseActivity activity;

    public UsersListAdapter(BaseActivity parentActivity){
        activity = parentActivity;
    }


    public class UserRowViewHolder extends RecyclerView.ViewHolder {
        UserRowBinding binding;
        public UserRowViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }
    }

    @Override
    public UserRowViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        UserRowBinding binding = UserRowBinding.inflate(inflater, parent, false);

        return new UserRowViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final User user = User.users.get(position);
        ((UserRowViewHolder)holder).binding.setUser(user);
        ((UserRowViewHolder) holder).binding.setClick(new UserRowInteractor() {
            @Override
            public void onDeleteUserClick(View v) {
                deleteUser(user);
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return User.users.size();
    }


    public void deleteUser(User user){
        activity.showProgress("Удаление пользователя "+user.name+"...");
        HttpRequester.getInstance().deleteUser(user);
        activity.updateUI();

    }
}
